# This example board has 6 MTP12 front panel connectors, imaginatively named 0, 1, 2, 3, 4 and 5 (other names can be used in reality though)
# RX FireFlys A, B and C connect to MTPs 0, 1 and 2 respectively.
# TX FireFlys D, E and F connect to MTPs 5, 4 and 3 respectively.
# The FireFly-MTP channel mapping is inverted for FireFlys A and D, but normal for the others.

# RX modules
0 MTP12   A.Rx11 A.Rx10 A.Rx9 A.Rx8   A.Rx7 A.Rx6 A.Rx5 A.Rx4   A.Rx3 A.Rx2 A.Rx1  A.Rx0
1 MTP12   B.Rx0  B.Rx1  B.Rx2 B.Rx3   B.Rx4 B.Rx5 B.Rx6 B.Rx7   B.Rx8 B.Rx9 B.Rx10 B.Rx11
2 MTP12   C.Rx0  C.Rx1  C.Rx2 C.Rx3   C.Rx4 C.Rx5 C.Rx6 C.Rx7   C.Rx8 C.Rx9 C.Rx10 C.Rx11

# TX modules
3 MTP12   F.Tx0  F.Tx1  F.Tx2 F.Tx3   F.Tx4 F.Tx5 F.Tx6 F.Tx7   F.Tx8 F.Tx9 F.Tx10 F.Tx11
4 MTP12   E.Tx0  E.Tx1  E.Tx2 E.Tx3   E.Tx4 E.Tx5 E.Tx6 E.Tx7   E.Tx8 E.Tx9 E.Tx10 E.Tx11
5 MTP12   D.Tx11 D.Tx10 D.Tx9 D.Tx8   D.Tx7 D.Tx6 D.Tx5 D.Tx4   D.Tx3 D.Tx2 D.Tx1  D.Tx0
