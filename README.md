# Example board plugin

This repository contains an example plugin for phase-2 boards, designed to be used as a basis
for creating plugins for real boards.

This plugin contains an example specialisation of each of the required interface classes. The
class implementations in this repository are all minimal skeletons, with placholders and other
important sections highlighted by 'FIXME' comments. For detailed instructions on how to create
a real plugin from this example, please read the "Creating a board plugin" page from the plugin
development guide at https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development


## Build instructions

*Prerequisites:* Install SWATCH, HERD and other dependencies, or use a development container
which already contains all dependencies, as described in 
https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html

To build this plugin:
```
mkdir build
cd build
cmake ..
make
```
