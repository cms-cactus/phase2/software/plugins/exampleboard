#include "exampleboard/swatch/InputPort.hpp"


namespace exampleboard {
namespace swatch {

using namespace ::swatch;

InputPort::InputPort(const size_t i, const phase2::ChannelID& connectedOpticsChannel) :
  phase2::InputPort(i, createId(i), connectedOpticsChannel)
// FIXME: Register metrics for monitoring data here
{
}


InputPort::~InputPort()
{
}


std::string InputPort::createId(const size_t i)
{
  std::ostringstream lOSS;
  lOSS << "Rx" << std::setw(2) << std::setfill('0') << i;
  return lOSS.str();
}


bool InputPort::checkPresence() const
{
  // FIXME: Return true if link firmware has been instantiated for this input channel
  //        in the currently-loaded FW; return false otherwise.
  return true;
}


void InputPort::retrievePropertyValues()
{
}


void InputPort::retrieveMetricValues()
{
  // FIXME: Read monitoring data from FPGA, and set values of corresponding metrics
  //        Including metrics from base class: mMetricIsLocked, mMetricIsAligned, mMetricCRCErrors, mMetricSourceLinkId, mMetricSourceBoardId
  // setMetric(mMetricIsLocked, VALUE);
  // setMetric(mMetricIsAligned, VALUE);
  // setMetric(mMetricCRCErrors, VALUE);
  // setMetric(mMetricSourceLinkId, VALUE);
  // setMetric(mMetricSourceBoardId, VALUE);
}

} // namespace swatch
} // namespace exampleboard
