
#include "exampleboard/swatch/FireflyTx.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace exampleboard {
namespace swatch {

FireflyTx::FireflyTx(const std::string& aId) :
  OpticalModule(aId, {} /* RX channel indices */, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 } /* TX channel indices */),
  mMetricTemperature(registerMetric<float>("temperature"))
// FIXME: Register other metrics for monitoring data here
{
  setWarningCondition(mMetricTemperature, ::swatch::core::GreaterThanCondition<float>(50));

  for (auto* channel : getOutputs())
    mMetricLaserFault[channel] = &channel->registerMetric<bool>("laserFault", "Laser fault", ::swatch::core::EqualCondition<bool>(true));
}


FireflyTx::~FireflyTx()
{
}


bool FireflyTx::checkPresence() const
{
  // FIXME: Check whether this FireFly site is currently populated - return true if it is, false if not.
  return true;
}


void FireflyTx::retrievePropertyValues()
{
}


void FireflyTx::retrieveMetricValues()
{
  // FIXME: Read monitoring data that's not channel-specific (e.g. temperature), and set values of corresponding metrics. E.g:
  // setMetric(mMetricRxPower, VALUE);
}


void FireflyTx::retrieveOutputMetricValues(Channel& aChannel)
{
  // FIXME: Read monitoring data for single channel (the channel with index aChannel.getIndex()), and set values of corresponding metrics. E.g:
  // setMetric(*mMetricLaserFault.at(&aChannel), VALUE);
}

} // namespace swatch
} // namespace exampleboard
