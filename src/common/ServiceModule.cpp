#include "exampleboard/swatch/ServiceModule.hpp"

#include "swatch/core/Factory.hpp"


SWATCH_REGISTER_CLASS(exampleboard::swatch::ServiceModule)

namespace exampleboard {
namespace swatch {

using namespace ::swatch;

ServiceModule::ServiceModule(const core::AbstractStub& aStub) :
  phase2::ServiceModule(aStub)
{
}


ServiceModule::~ServiceModule()
{
}


void ServiceModule::retrieveMetricValues()
{
}

} // namespace swatch
} // namespace exampleboard
