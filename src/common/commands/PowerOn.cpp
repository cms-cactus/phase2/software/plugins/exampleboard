#include "exampleboard/swatch/commands/PowerOn.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

using namespace ::swatch;


PowerOn::PowerOn(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Power on", aActionable, std::string())
{
  // FIXME: Add any parameters here
}


PowerOn::~PowerOn()
{
}


action::Command::State PowerOn::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from SWATCH-independent board control libraries
  //        that power on data-processing FPGA (and supporting components)
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function


  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if FPGA powered on but something abnormal and worrisome happened
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace exampleboard
