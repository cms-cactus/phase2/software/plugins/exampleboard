#include "exampleboard/swatch/commands/Program.hpp"


#include "swatch/action/File.hpp"
#include "swatch/phase2/Processor.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

using namespace ::swatch;


Program::Program(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Program FPGA", aActionable, std::string())
{
  // Firmware package containing bitstream and any other associated files
  registerParameter<action::File>("package", { "/path/to/firmwarePackage.tgz", "", "" });
  // FIXME: Add any other parameters here
}


Program::~Program()
{
}


action::Command::State Program::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from SWATCH-independent board control libraries
  //        that program data-processing FPGA (and supporting components)
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function


  // Declare that main FW components are non-functional after programming (until configured by other commands)
  using namespace ::swatch::phase2::processorIds;
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts, kTTC });

  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if FPGA powered on but something abnormal and worrisome happened
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace exampleboard
