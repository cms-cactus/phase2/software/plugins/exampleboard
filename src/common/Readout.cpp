#include "exampleboard/swatch/Readout.hpp"


namespace exampleboard {
namespace swatch {

using namespace ::swatch;

Readout::Readout() :
  ReadoutInterface()
// FIXME: Register metrics for monitoring data here (in long term)
{
}


Readout::~Readout()
{
}


void Readout::retrieveMetricValues()
{
  // FIXME: Read monitoring data from FPGA, and set values of corresponding metrics (in long term)
}

} // namespace swatch
} // namespace exampleboard
