
#include "exampleboard/swatch/FireflyRx.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace exampleboard {
namespace swatch {

FireflyRx::FireflyRx(const std::string& aId) :
  OpticalModule(aId, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 } /* RX channel indices */, {} /* TX channel indices */),
  mMetricTemperature(registerMetric<float>("temperature"))
// FIXME: Register other metrics for monitoring data here
{
  setWarningCondition(mMetricTemperature, ::swatch::core::GreaterThanCondition<float>(50));

  for (auto* channel : getInputs())
    mMetricRxPower[channel] = &channel->registerMetric<float>("power", "Power (mW)");
}


FireflyRx::~FireflyRx()
{
}


bool FireflyRx::checkPresence() const
{
  // FIXME: Check whether this FireFly site is currently populated - return true if it is, false if not.
  return true;
}


void FireflyRx::retrievePropertyValues()
{
}


void FireflyRx::retrieveMetricValues()
{
  // FIXME: Read monitoring data that's not channel-specific (e.g. temperature), and set values of corresponding metrics. E.g:
  // setMetric(mMetricTemperature, VALUE);
}


void FireflyRx::retrieveInputMetricValues(Channel& aChannel)
{
  // FIXME: Read monitoring data for single channel (the channel with index aChannel.getIndex()), and set values of corresponding metrics. E.g:
  // setMetric(*mMetricRxPower.at(&aChannel), VALUE);
}

} // namespace swatch
} // namespace exampleboard
