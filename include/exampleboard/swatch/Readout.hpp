#ifndef __EXAMPLEBOARD_SWATCH_READOUT_HPP__
#define __EXAMPLEBOARD_SWATCH_READOUT_HPP__

#include "swatch/phase2/ReadoutInterface.hpp"


namespace exampleboard {
namespace swatch {

// Represents SLink interface and any associated readout FW infrastructure
class Readout : public ::swatch::phase2::ReadoutInterface {
public:
  Readout();
  ~Readout();

private:
  void retrieveMetricValues() final;

  // FIXME: In long term, add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing your monitoring data for SLink interface and associated readout FW
};

} // namespace swatch
} // namespace exampleboard


#endif