#ifndef __EXAMPLEBOARD_SWATCH_PROCESSOR_HPP__
#define __EXAMPLEBOARD_SWATCH_PROCESSOR_HPP__

#include "swatch/phase2/Processor.hpp"


namespace exampleboard {
namespace swatch {

// Represents a data-processing FPGA that implements the main function of a board (e.g. the trigger algorithm)
class Processor : public ::swatch::phase2::Processor {
public:
  Processor(const ::swatch::core::AbstractStub& aStub);
  ~Processor();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&) representing any FPGA-level
  //        monitoring data that doesn't fit neatly into I/O, TCDS or Readout classes
};

} // namespace swatch
} // namespace exampleboard


#endif