#ifndef __EXAMPLEBOARD_SWATCH_COMMANDS_POWERON_HPP__
#define __EXAMPLEBOARD_SWATCH_COMMANDS_POWERON_HPP__

#include "swatch/action/Command.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

class PowerOn : public ::swatch::action::Command {
public:
  PowerOn(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~PowerOn();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace exampleboard


#endif