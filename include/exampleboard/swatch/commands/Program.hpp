#ifndef __EXAMPLEBOARD_SWATCH_COMMANDS_PROGRAM_HPP__
#define __EXAMPLEBOARD_SWATCH_COMMANDS_PROGRAM_HPP__

#include "swatch/action/Command.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

class Program : public ::swatch::action::Command {
public:
  Program(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~Program();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace exampleboard


#endif