#ifndef __EXAMPLEBOARD_SWATCH_TCDS_HPP__
#define __EXAMPLEBOARD_SWATCH_TCDS_HPP__

#include "swatch/phase2/TTCInterface.hpp"


namespace exampleboard {
namespace swatch {

// Represents TCDS2 interface (and FPGA-local TCDS generator logic if present)
class TCDS : public ::swatch::phase2::TTCInterface {
public:
  TCDS();
  ~TCDS();

private:
  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing your monitoring data for TCDS2 interface (and any FPGA-local generator logic)
};

} // namespace swatch
} // namespace exampleboard


#endif
