#ifndef __EXAMPLEBOARD_SWATCH_OUTPUTPORT_HPP__
#define __EXAMPLEBOARD_SWATCH_OUTPUTPORT_HPP__

#include "swatch/phase2/OutputPort.hpp"


namespace exampleboard {
namespace swatch {

// Represents an MGT TX port (and associated firmware implementing link protocol)
class OutputPort : public ::swatch::phase2::OutputPort {
public:
  OutputPort(const size_t, const ::swatch::phase2::ChannelID&);

  ~OutputPort();

private:
  static std::string createId(const size_t);

  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing your monitoring data for MGT TX ports
};

} // namespace swatch
} // namespace exampleboard


#endif